package capgem.yrolnline.com.capgem_android.utils;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Toast;
import java.util.ArrayList;

import capgem.yrolnline.com.capgem_android.DetailActivity;
import capgem.yrolnline.com.capgem_android.FeedViewHolder;
import capgem.yrolnline.com.capgem_android.ItemClickListener;
import capgem.yrolnline.com.capgem_android.R;

/**
 * Created by yrolfernando on 3/26/18.
 */

public class FeedAdaptor extends BaseAdapter {

    Context context;
    ArrayList<FeedObj> feed;
    LayoutInflater inflater;

    public FeedAdaptor(Context context, ArrayList<FeedObj> feed) {
        this.context = context;
        this.feed = feed;
    }

    @Override
    public int getCount() {
        return feed.size();
    }
    @Override
    public Object getItem(int position) {
        return feed.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if(inflater == null) {
            inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        convertView = inflater.inflate(R.layout.feed_model_primary, parent,false);

        //setting textual properties
        FeedViewHolder feedViewHolder = new FeedViewHolder(convertView);
        feedViewHolder.feedTitle.setText(feed.get(position).getTitle());
        feedViewHolder.feedDescription.setText(feed.get(position).getDescription());

        //assign image using Async Class "ImageDownloader"
        if(feedViewHolder.feedImage != null && feed.get(position).getImage() != null){
            new ImageDownloader(feedViewHolder.feedImage, context).execute(feed.get(position).getImage());
        }

        //adding the click event
        feedViewHolder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick() {
                Intent intent = new Intent(context, DetailActivity.class);
                intent.putExtra("feedTitle", feed.get(position).getTitle());
                intent.putExtra("feedDescription", feed.get(position).getDescription());
                context.startActivity(intent);
            }
        });
        return convertView;
    }

}
