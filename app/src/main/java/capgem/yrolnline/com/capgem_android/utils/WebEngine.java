package capgem.yrolnline.com.capgem_android.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;

/**
 * Created by yrolfernando on 3/26/18.
 */


public class WebEngine {

    private Parser parser;

    //fetch request using Volley
    public void fetchFeed(Context context, final Activity activity){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                //start parsing information
                parser = new Parser();
                ArrayList<FeedObj> feedObjects = parser.parseFeed(response.toString());

                if(feedObjects.size() > 0){

                    //serialize the feed ArrayList and send to main Activity
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Constants.FEED_OBJECTS_BUNDLE, feedObjects);
                    Intent intent = new Intent(Constants.BROADCAST_FETCH_SUCCESS);
                    intent.putExtra(Constants.FEED_OBJECTS_EXTRA, bundle);
                    activity.sendBroadcast(intent);

                }else{
                    Intent intent = new Intent(Constants.BROADCAST_FETCH_FAILURE);
                    intent.putExtra(Constants.FEED_OBJECTS_EXTRA, "No results found");
                    activity.sendBroadcast(intent);
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String errorString = "An unknown error occurred. Please try again.";

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            errorString = "Connection error occurred. Please try again.";
                        } else if (error instanceof AuthFailureError) {
                            errorString = "Authentication error occurred. Please try again.";
                        } else if (error instanceof ServerError) {
                            errorString = "Server error occurred. Please try again.";
                        } else if (error instanceof NetworkError) {
                            errorString = "Network error occurred. Please try again.";
                        } else if (error instanceof ParseError) {
                            errorString = "Parse error occurred. Please try again.";
                        }

                        //Failure block due to network errors & etc
                        Intent intent = new Intent(Constants.BROADCAST_FETCH_FAILURE);
                        intent.putExtra(Constants.FEED_OBJECTS_EXTRA, errorString);
                        activity.sendBroadcast(intent);
                    }
                }){
        };

        //start the fetch request
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }
}