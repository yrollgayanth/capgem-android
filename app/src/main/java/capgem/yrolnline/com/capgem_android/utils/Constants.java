package capgem.yrolnline.com.capgem_android.utils;

/**
 * Created by yrolfernando on 3/26/18.
 */

public class Constants {
    //API URL
    public static final String BASE_URL = "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json";

    //Broadcastreceiver names
    public static final String BROADCAST_FETCH_SUCCESS = "BROADCAST_FETCH_SUCCESS";
    public static final String BROADCAST_FETCH_FAILURE = "BROADCAST_FETCH_FAILURE";

    //intent constants
    public static final String FEED_OBJECTS_EXTRA = "NEWS_OBJECTS_EXTRA";
    public static final String FEED_OBJECTS_BUNDLE  = "NEWS_OBJECTS_BUNDLE";

}
