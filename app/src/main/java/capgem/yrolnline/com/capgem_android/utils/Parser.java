package capgem.yrolnline.com.capgem_android.utils;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by yrolfernando on 3/26/18.
 */

public class Parser {

    public ArrayList parseFeed(String jsonString) {

        ArrayList<FeedObj> feedList = new ArrayList<FeedObj>();

        try {
            JSONObject mainJsonObj = new JSONObject(jsonString);

            JSONArray feedItemsArr = mainJsonObj.getJSONArray("rows");

            if(feedItemsArr.length() > 0) {

                //loop through the objects
                for(int i=0; i<feedItemsArr.length(); i++) {

                    JSONObject singleFeedItem = feedItemsArr.getJSONObject(i);

                    //create a feed object at each iteration
                    FeedObj feedObj = new FeedObj();
                    feedObj.setTitle(singleFeedItem.getString("title"));
                    feedObj.setDescription(singleFeedItem.getString("description"));

//                    if( singleFeedItem.getString("imageHref").trim().toString() == "null"){
//                        feedObj.setImage("http://www.donegalhimalayans.com/images/That%20fish%20was%20this%20big.jpg");
//                    }else{
//                        feedObj.setImage(singleFeedItem.getString("imageHref"));
//                    }

                    feedObj.setImage(singleFeedItem.getString("imageHref"));


                    //add feed object to ArrayList
                    feedList.add(feedObj);
                }
            }
            return feedList;

        }catch(final JSONException e){
            Log.v("PARSER ERR", e.getLocalizedMessage());
            return feedList;
        }
    }
}
