package capgem.yrolnline.com.capgem_android.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.ImageView;

import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import capgem.yrolnline.com.capgem_android.R;

/**
 * Created by yrolfernando on 3/26/18.
 * AsyncTask class for downloading images from the internet
 */

public class ImageDownloader extends AsyncTask<String, Void, Bitmap> {

    private Context context;

    //define WeakReference to ensure ImageView is not forced to remain in memory
    private final WeakReference<ImageView> imageViewReference;

    public ImageDownloader(ImageView imageView, Context context) {
        this.context = context;
        imageViewReference = new WeakReference<>(imageView);
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        return downloadBitmap(params[0]);
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if (isCancelled()) {
            bitmap = null;
        }

        if (imageViewReference != null) {//check if image view object still in the memory
            ImageView imageView = imageViewReference.get();
            if (imageView != null) {
                if (bitmap != null) {
                    imageView.setImageBitmap(bitmap);
                } else {
                    Drawable placeholder = ContextCompat.getDrawable(context, R.drawable.default_image);
                    imageView.setImageDrawable(placeholder);
                }
            }

        }
    }

    //download image through HTTP request using HttpURLConnection and returns a Bitmap
    private Bitmap downloadBitmap(String url) {
        HttpURLConnection urlConnection = null;

        try {
            URL uri = new URL(url);
            urlConnection = (HttpURLConnection) uri.openConnection();

            int statusCode = urlConnection.getResponseCode();
            if (statusCode != HttpURLConnection.HTTP_OK) {
                return null;
            }

            InputStream inputStream = urlConnection.getInputStream();
            if (inputStream != null) {
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                return bitmap;
            }
        } catch (Exception e) {
            urlConnection.disconnect();
            Log.v("ImageDownloader", "An error occurred downloading the image from " + url);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return null;
    }
}
