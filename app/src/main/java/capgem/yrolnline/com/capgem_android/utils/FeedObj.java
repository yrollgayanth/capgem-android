package capgem.yrolnline.com.capgem_android.utils;

import java.io.Serializable;

/**
 * Created by yrolfernando on 3/26/18.
 */

public class FeedObj implements Serializable {
    private String title;
    private String description;
    private String imageHref;

    //setters
    public void setTitle(String title){
        this.title =  title;
    }

    public void setImage(String image){
        this.imageHref = image;
    }

    public void setDescription(String description){
        this.description = description;
    }

    //getters
    public String getTitle(){
        return this.title;
    }
    public String getImage(){ return this.imageHref; }
    public String getDescription(){
        return this.description;
    }

}
