package capgem.yrolnline.com.capgem_android;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by yrolfernando on 3/26/18.
 */

public class FeedViewHolder implements View.OnClickListener {

    public ImageView feedImage;
    public TextView feedTitle;
    public TextView feedDescription;
    public ItemClickListener itemClickListener;

    public FeedViewHolder(View v) {
        feedImage = (ImageView) v.findViewById(R.id.feedImage);
        feedTitle = (TextView) v.findViewById(R.id.feedTitle);
        feedDescription = (TextView) v.findViewById(R.id.feedDescription);
        v.setOnClickListener(this);
    }

    public  void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {
        this.itemClickListener.onItemClick();
    }
}
