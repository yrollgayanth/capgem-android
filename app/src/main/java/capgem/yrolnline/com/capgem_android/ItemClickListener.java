package capgem.yrolnline.com.capgem_android;

/**
 * Created by yrolfernando on 3/26/18.
 */

public interface ItemClickListener {
    void onItemClick();
}

