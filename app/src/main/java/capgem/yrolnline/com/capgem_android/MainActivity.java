package capgem.yrolnline.com.capgem_android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import capgem.yrolnline.com.capgem_android.utils.Constants;
import capgem.yrolnline.com.capgem_android.utils.FeedAdaptor;
import capgem.yrolnline.com.capgem_android.utils.FeedObj;
import capgem.yrolnline.com.capgem_android.utils.Utils;
import capgem.yrolnline.com.capgem_android.utils.WebEngine;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private Utils utils;
    private WebEngine webEngine;

    //variables of the ListView
    ListView listView;
    FeedAdaptor feedAdaptor;
    SwipeRefreshLayout swipeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //swipe layout
        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swiper);
        swipeLayout.setOnRefreshListener(MainActivity.this);

        //set up the customizable toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Feed");
        toolbar.setTitleTextColor(ContextCompat.getColor(this.getApplication(), R.color.colorTitleBlue));

        //set supportive classes
        utils = new Utils();
        webEngine = new WebEngine();

        //start feed fetch
        startFeedFetch();
    }

    //start fetching feed
    private void startFeedFetch(){
        if(utils.isConnectedToInternet(getApplicationContext())) {
            webEngine.fetchFeed(getApplicationContext(), MainActivity.this);
        }else{
            Toast.makeText(MainActivity.this, "Please turn ON your internet and try again.", Toast.LENGTH_SHORT).show();
            swipeLayout.setRefreshing(false);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        //register broadcastreceivers on resume (which will also get called on create)
        registerReceiver(fetchResponseSuccess, new IntentFilter(Constants.BROADCAST_FETCH_SUCCESS));
        registerReceiver(fetchResponseFailure, new IntentFilter(Constants.BROADCAST_FETCH_FAILURE));

        //start fetching feed on resume
        startFeedFetch();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        //unregister broadcast receivers on destroy
        unregisterReceiver(fetchResponseSuccess);
        unregisterReceiver(fetchResponseFailure);
    }

    //broadcast for receiving success feed fetch response
    BroadcastReceiver fetchResponseSuccess = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            //fetch information on success and inject into the ListView
            Bundle extra = intent.getBundleExtra(Constants.FEED_OBJECTS_EXTRA);
            ArrayList<FeedObj> feedObjects = (ArrayList<FeedObj>) extra.getSerializable(Constants.FEED_OBJECTS_BUNDLE);
            listView = (ListView) findViewById(R.id.feedListView);
            listView.setDivider(null);
            feedAdaptor = new FeedAdaptor(MainActivity.this, feedObjects);
            listView.setAdapter(feedAdaptor);

            //remove swipe animation if applicable
            swipeLayout.setRefreshing(false);
        }
    };

    //broadcast for failing to fetch feed
    BroadcastReceiver fetchResponseFailure = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            //remove swipe animation if applicable
            swipeLayout.setRefreshing(false);
            Toast.makeText(MainActivity.this, "An error occurred while fetching feed. Please try again.", Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    public void onRefresh() {
        startFeedFetch();
    }

}
