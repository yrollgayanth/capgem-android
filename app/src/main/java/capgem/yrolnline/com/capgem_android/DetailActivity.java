package capgem.yrolnline.com.capgem_android;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    private TextView bodyText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        //body text
        bodyText = (TextView)findViewById(R.id.articleBody);

        //get extra values
        Intent intent = getIntent();
        String feedTitle = intent.getStringExtra("feedTitle");
        String feedDescription = intent.getStringExtra("feedDescription");

        bodyText.setText(feedDescription);

        setTitle(feedTitle);

    }
}
